﻿using Api.Cheque.Request;
using Cheque.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Text.RegularExpressions;

namespace Api.Cheque.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChequeController : Controller
    {
        private readonly ILogger<ChequeController> _logger;
        private readonly IChequeService _chequeService;

        public ChequeController(ILogger<ChequeController> logger, IChequeService chequeService)
        {
            _logger = logger;
            _chequeService = chequeService;
        }
        /// <summary>
        /// Get amount in words
        /// REMARK : Improved with common request , response formats. Skipping due to time limitation.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("{Amount}")]
        public IActionResult GetAmountInWords([FromRoute] GetAmountInWordsRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var amountInWords = _chequeService.GetAmountInWords(decimal.Parse(request.Amount));
                    return Ok(amountInWords);
                }
                catch (Exception ex)
                {
                    _logger.LogError("Get amount in words : " + ex.Message);
                    return StatusCode(500, ex.Message);
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            
        }
    }
}
