﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Cheque.Request
{
    public class GetAmountInWordsRequest : IValidatableObject
    {
        [Required]
        public string Amount { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            decimal d;
            if (!decimal.TryParse(Amount, out d))
            {
                yield return new ValidationResult("Amount is not a valid number");
            }
            else if (!(d > 0.0m))
            {
                if (d == 0.0m)
                    yield return new ValidationResult("Value Zero is not permitted");
                else
                    yield return new ValidationResult("Negative Value is not permitted");
            }
            
           
        }

    }
}
