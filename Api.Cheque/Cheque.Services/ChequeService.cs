﻿using System;

namespace Cheque.Services
{
    public class ChequeService : IChequeService
    {
        public string GetAmountInWords(decimal amount)
        {
            var amountInWords = AmountToWords((Math.Round(amount, 2, MidpointRounding.ToZero)).ToString());
            return amountInWords;
        }

        /// <summary>
        /// Number converted to words in currency terms
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>Amount in words</returns>
        private string AmountToWords(string amount)
        {
            var splits = amount.Split('.');
            string amountInDollar;
            string amountIncents = string.Empty;
            int dollar = Convert.ToInt32(splits[0]);
            int cents = (splits.Length == 2) ? Convert.ToInt32(splits[1]) : 0;

            amountInDollar = (dollar > 0) ? NumberToWord(dollar) : "";
            amountIncents = (cents > 0) ? NumberToWord(Convert.ToInt32(splits[1])) : "";

            return ($"{(!string.IsNullOrEmpty(amountInDollar) ? amountInDollar + ((dollar==1)?" DOLLAR":" DOLLARS") : "")}" +
                    $"{(!string.IsNullOrEmpty(amountInDollar) && !string.IsNullOrEmpty(amountIncents) ? " AND " : "")}" +
                    $"{(!string.IsNullOrEmpty(amountIncents) ? amountIncents + ((cents == 1) ? " CENT" : " CENTS") : "")}" +
                    $"{(string.IsNullOrEmpty(amountInDollar) ? " ONLY" : "")}").Trim();
        }

        /// <summary>
        /// Convert given number to word
        /// </summary>
        /// <param name="inputNum"></param>
        /// <returns></returns>
        private string NumberToWord(int inputNum)
        {
            int dig1, dig2, dig3, level = 0, lasttwo, threeDigits;
            string numInWords = string.Empty;
            string[] ones = { "", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
            string[] tens = { "", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };
            string[] thou = { "", "THOUSAND", "MILLION", "BILLION", "TRILLION", "QUADRILLION", "QUINTILLION" };

            string num = inputNum.ToString();

            while (num.Length > 0)
            {
                //Get the three rightmost characters
                string x = (num.Length < 3) ? num : num.Substring(num.Length - 3, 3);

                // Separate the three digits
                threeDigits = int.Parse(x);
                lasttwo = threeDigits % 100;
                dig1 = threeDigits / 100;
                dig2 = lasttwo / 10;
                dig3 = (threeDigits % 10);

                // append a "thousand" where appropriate
                if (level > 0 && dig1 + dig2 + dig3 > 0)
                {
                    numInWords = thou[level] + " " + numInWords;
                    numInWords = numInWords.Trim();
                }

                // check that the last two digits is not a zero
                if (lasttwo > 0)
                {
                    if (lasttwo < 20)
                    {
                        // if less than 20, use "ones" only
                        numInWords = ones[lasttwo] + " " + numInWords;
                    }
                    else
                    {
                        // otherwise, use both "tens" and "ones" array
                        numInWords = tens[dig2] + "-" + ones[dig3] + " " + numInWords;
                    }
                    if (num.Length < 3)
                    {
                        return numInWords.Trim();
                    }
                }

                // if a hundreds part is there, translate it
                if (dig1 > 0)
                {
                    numInWords = ones[dig1] + " HUNDRED " + numInWords;
                    num = (num.Length - 3) > 0 ? num.Substring(0, num.Length - 3) : "";
                    level++;
                }
                else
                {
                    if (num.Length > 3)
                    {
                        num = num.Substring(0, num.Length - 3);
                        level++;
                    }
                }
            }
            return numInWords.Trim();

        }
    }
}
