﻿namespace Cheque.Services
{
    public interface IChequeService
    {
        string GetAmountInWords(decimal amount);
    }
}
