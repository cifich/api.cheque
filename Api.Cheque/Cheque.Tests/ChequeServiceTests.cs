using Cheque.Services;
using System;
using Xunit;

namespace Cheque.Tests
{
    public class ChequeServiceTests
    {
        private readonly ChequeService chequeService = new ChequeService();

        [Theory]
        [InlineData(68,"SIXTY-EIGHT DOLLARS")]
        [InlineData(1234.56, "ONE THOUSAND TWO HUNDRED THIRTY-FOUR DOLLARS AND FIFTY-SIX CENTS")]
        [InlineData(0.22, "TWENTY-TWO CENTS ONLY")]
        [InlineData(102.03, "ONE HUNDRED TWO DOLLARS AND THREE CENTS")]
        [InlineData(1.021, "ONE DOLLAR AND TWO CENTS")]
        [InlineData(1.23987, "ONE DOLLAR AND TWENTY-THREE CENTS")]
        public void Cheque_service_should_convert_valid_number_to_word_in_currency_format(decimal number, string expectedValue)
        {
            //Arrange, Act
            var s = chequeService.GetAmountInWords(number);
            var f = String.Equals(s, expectedValue);
            //Assert 
            Assert.Equal(s, expectedValue);
        }
    }
}
